import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private http: HttpClient) { }

  //get method
  async  gettodoData() {
    return await this.http.get('https://jsonplaceholder.typicode.com' + '/' + 'todos').toPromise();
  }

  //post method 
  async saveTodoData(info) {
    let headers = new HttpHeaders({
      'content-Type': 'application/json'
    })
    return await this.http.post('https://jsonplaceholder.typicode.com' + '/' + 'todos', info, { headers: headers }).toPromise();
  }

  // get method by id
  async gettododataById(userid) {
    let headers = new HttpHeaders({
      'content-Type': 'application/json'
    })
    return await this.http.get('https://jsonplaceholder.typicode.com' + '/' + 'todos' + '/' + userid, { headers: headers }).toPromise();
  }

  // put method
  async updateTodoData(userid, info) {
    let headers = new HttpHeaders({
      'content-Type': 'application/json'
    })
    return await this.http.put('https://jsonplaceholder.typicode.com' + '/' + 'todos' + '/' + userid, info, { headers: headers }).toPromise();
  }

  // delete method
  async deleteTodoData(userid) {
    let headers = new HttpHeaders({
      'content-Type': 'application/json'
    })
    return await this.http.delete('https://jsonplaceholder.typicode.com' + '/' + 'todos' + '/' + userid, { headers: headers }).toPromise();
  }
}
