import { Component, OnInit } from '@angular/core';
import { TodosService } from '../todos.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  tododata: any;
  enteredid: any;
  entereduserid: any;
  enteredtitle: any;
  enteredtask: any;
  createdtodoData: any;
  getTodoResData: any;
  selectedtodoId: any;
  updateTodoResData: any;
  deleteTodoResData: any;

  constructor(private authservice: TodosService) { }

  ngOnInit() {
    this.getAllDisplayInfo();
  }

  // get 
  async getAllDisplayInfo() {
    this.tododata = await this.authservice.gettodoData();
    console.log('get tododata:', this.tododata);
  }

  //post
  savetodoList() {
    var createData = {
      'id': this.enteredid,
      'userId': this.entereduserid,
      'title': this.enteredtitle,
      'completed': this.enteredtask
    }
    this.authservice.saveTodoData(createData).then((data) => {
      this.createdtodoData = data;
      console.log('created tododata:', this.createdtodoData);
    })
  }

  // get by id
  DisplaydataById(id) {
    this.authservice.gettododataById(id).then((data) => {
      this.getTodoResData = data;
      console.log('got id data:', this.getTodoResData);

      this.enteredid = this.getTodoResData.id;
      this.entereduserid = this.getTodoResData.userId;
      this.enteredtitle = this.getTodoResData.title;
      this.enteredtask = this.getTodoResData.completed;

      this.selectedtodoId = this.getTodoResData.id;
      console.log('get by id:', this.selectedtodoId);
    });
  }

  // put 
  updateTodoInfo() {
    var updateData = {
      'id': this.enteredid,
      'userId': this.entereduserid,
      'title': this.enteredtitle,
      'completed': this.enteredtask
    }
    console.log('updatedtodo data:', updateData);

    this.authservice.updateTodoData(this.selectedtodoId, updateData).then((data) => {
      this.updateTodoResData = data;
      console.log('updatedtodo info:', this.updateTodoInfo);
    })
  }

  // delete 
  deleteTodoInfo(id) {
    this.authservice.deleteTodoData(id).then((data) => {
      this.deleteTodoResData = data;
      console.log('deleted todoData:', this.deleteTodoResData);
    })
  }
}

